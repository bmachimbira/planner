package zm.co.goalgetter.planner.api

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.model.TaskModel
import zm.co.goalgetter.planner.repository.TaskRepository
import zm.co.goalgetter.planner.repository.GoalRepository

class TaskApi constructor(var taskRepository: TaskRepository, var goalRepository: GoalRepository){

    fun saveTask(task: Task){
        taskRepository.insert(task)
    }

    fun getAllTasks(): RealmResults<TaskModel>{
       return  taskRepository.getAllTasks()
    }

    fun getAllGoals():Map<String, String>{
        var goalTitles = mutableMapOf<String, String>()
        for (goal in goalRepository.getAll()){
            goalTitles.put(goal.id!!, goal.specific!!)
        }
        return goalTitles
    }

    fun getTasksByGoal(goalId: String): RealmResults<TaskModel>{
        return  taskRepository.getTasksById(goalId)
    }

    fun getTask(taskId: String): Task {
        return taskRepository.getById(taskId)
    }

    fun getTaskFromModel(taskModel: TaskModel?): Task {
        return taskRepository.getTaskFromModel(taskModel)
    }

    fun updateTask(task: Task){
        taskRepository.update(task)
    }


}
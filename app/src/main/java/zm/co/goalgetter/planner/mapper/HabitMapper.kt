package zm.co.goalgetter.planner.mapper

import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.model.HabitModel

class HabitMapper {
    fun mapToDomain(habit: HabitModel): Habit {
        return Habit(id = habit.id!!,
                category = habit.category,
                rating = habit.rating,
                state = habit.state,
                costToMe = habit.costToMe,
                obstacles = habit.obstacles,
                goal = habit.goal,
                newHabit = habit.newHabit,
                resourcesNeeded = habit.resourcesNeeded)
    }


    fun mapToDomainList(habitList: List<HabitModel>): List<Habit>{
        var domainList = mutableListOf<Habit>()
        habitList.forEach { habit ->
            domainList.add(mapToDomain(habit))
        }
        return domainList
    }
}
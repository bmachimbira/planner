package zm.co.goalgetter.planner.view.quarterly.viewAssessments.list

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.model.HabitModel

interface ViewAssessmentListContract {
    interface UserActions {
        fun initialiseList()
        fun getItemAtIndex(position: Int)

    }

    interface View {
        fun showDetail(habit: Habit)
        fun setAdapter(habits: RealmResults<HabitModel>)
    }
}

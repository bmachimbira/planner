package zm.co.goalgetter.planner.domain

class Review constructor() {
    var id: String? = null
    var goal: String? = null
    var performance: String? = null
    var highlights: String? = null
    var lessons: String? = null
    var obstacles: String? = null
    var howDoIDoBetter: String? = null

    constructor(id: String,
                goal: String,
                performance: String,
                highlights: String,
                lessons: String,
                obstacles: String,
                howDoIDoBetter: String) : this() {
        this.id = id
        this.goal = goal
        this.performance = performance
        this.highlights = highlights
        this.lessons = lessons
        this.obstacles = obstacles
        this.howDoIDoBetter = howDoIDoBetter
    }
}
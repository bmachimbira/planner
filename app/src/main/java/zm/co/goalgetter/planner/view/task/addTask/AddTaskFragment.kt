package zm.co.goalgetter.planner.view.task.addTask

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import io.realm.Realm
import kotlinx.android.synthetic.main.content_add_task.*
import kotlinx.android.synthetic.main.content_add_task.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.TaskApi
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.mapper.TaskMapper
import zm.co.goalgetter.planner.repository.GoalRepository
import zm.co.goalgetter.planner.repository.TaskRepository
import zm.co.goalgetter.planner.view.goal.viewGoals.GoalTaskListFragment
import java.util.*

class AddTaskFragment : Fragment(), AddTaskContract.View {
    lateinit private var adapter: ArrayAdapter<String>

    lateinit private var presenter: AddTaskContract.Presenter
    lateinit private var goalListSpinner: Spinner
    lateinit private var saveTaskButton: FloatingActionButton

    companion object {
        fun newInstance():Fragment{
            return AddTaskFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.activity_add_task, container, false)
        this. presenter = AddTaskPresenter(this,
                TaskApi(TaskRepository(Realm.getDefaultInstance(), TaskMapper()),
                        GoalRepository(Realm.getDefaultInstance(), GoalMapper())))

        this.goalListSpinner = view.goalListSpinner
        this.saveTaskButton = view.saveTaskButton
        presenter.getGoals()

        saveTaskButton.setOnClickListener { saveTask(view = view) }
        saveTaskButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_add_circle_black_24dp))

        return view
    }

    override fun onResume() {
        super.onResume()
        saveTaskButton.show()
    }

    private fun showFragment(fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(taskContainerFragment.id, fragment)
                .addToBackStack("Goal")
                .commit()
        saveTaskButton.hide()
    }

    private fun saveTask(view: View) {
        val goal = adapter.getItem(view.goalListSpinner.selectedItemPosition)
        val year = view.taskDueTimeline.selectedYear
        val month = view.taskDueTimeline.selectedMonth
        val day = view.taskDueTimeline.selectedDay

        val date = GregorianCalendar(year, month, day).time
        presenter.saveTask(
                goal,
                view.taskTitleInput.text.toString(),
                view.descriptionInput.text.toString(),
                date)
    }


    override fun populateGoalList(goals: List<String>) {
        adapter = ArrayAdapter(activity, R.layout.spinner_item, goals)
        this.goalListSpinner.adapter = adapter
    }

    override fun endFragment() {
        activity.onBackPressed()
    }
}

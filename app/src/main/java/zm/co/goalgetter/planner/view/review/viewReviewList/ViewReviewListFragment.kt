package zm.co.goalgetter.planner.view.review.viewReviewList


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_view_review_list.*
import kotlinx.android.synthetic.main.fragment_view_review_list.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.ReviewApi
import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.mapper.ReviewMapper
import zm.co.goalgetter.planner.model.ReviewModel
import zm.co.goalgetter.planner.repository.ReviewRepository
import zm.co.goalgetter.planner.view.review.addReview.AddReviewFragment
import zm.co.goalgetter.planner.view.review.viewReviewDetail.ViewReviewDetailFragment

class ViewReviewListFragment : Fragment(), ViewReviewListContract.View {

    companion object {
        fun newInstance(): Fragment {
            return ViewReviewListFragment()
        }
    }

    private lateinit var presenter: ViewReviewListContract.UserActions
    private lateinit var reviewsList: RecyclerView
    private lateinit var addReviewFab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.presenter = ViewReviewListPresenter(
                view = this,
                reviewApi = ReviewApi(
                        reviewRepository = ReviewRepository(
                                realm = Realm.getDefaultInstance(),
                                mapper = ReviewMapper()
                        )))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_view_review_list, container, false)
        this.reviewsList = view.review_recycler_view
        this.addReviewFab = view.addReviewFab
        addReviewFab.setOnClickListener {
            createReview()
        }
        return view
    }

    private fun createReview() {
        val addReviewFragment = AddReviewFragment.newInstance()
        showFragment(fragment = addReviewFragment)
    }

    override fun onResume() {
        super.onResume()
        presenter.initialiseList()
    }

    override fun showDetail(review: Review) {
        val reviewDetailFragment = ViewReviewDetailFragment.newInstance(review)
        showFragment(fragment = reviewDetailFragment)
    }

    private fun showFragment(fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(reviewContainer.id, fragment)
                .addToBackStack("Review")
                .commit()
        addReviewFab.hide()
    }

    private val itemOnClick: (View, Int, Int) -> Unit = { _, position, _ ->
        presenter.getItemAtIndex(position)
    }

    override fun loadReviewsIntoViews(reviews: RealmResults<ReviewModel>) {
        val adapter = ViewReviewsRecyclerViewAdapter(reviews, itemOnClick)
        reviewsList.layoutManager = LinearLayoutManager(activity)
        reviewsList.adapter = adapter
        reviewsList.setHasFixedSize(true)
    }


}

package zm.co.goalgetter.planner.model

import io.realm.RealmObject

open class HabitModel : RealmObject() {
    var id: String? = null
    var category: String? = null
    var rating: String? = null
    var state: String? = null
    var costToMe: String? = null
    var obstacles: String? = null
    var goal: String? = null
    var newHabit: String? = null
    var resourcesNeeded: String? = null
}
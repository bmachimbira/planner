package zm.co.goalgetter.planner.view.quarterly.viewAssessments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.goal_list_content.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.model.HabitModel

internal class ViewHabitsRecyclerViewAdapter(data: OrderedRealmCollection<HabitModel>,
                                             val itemClickListener: (View, Int, Int) -> Unit) :
        RealmRecyclerViewAdapter<HabitModel, ViewHabitsRecyclerViewAdapter.HabitViewHolder>(data, true) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HabitViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.goal_list_content, parent, false)

        val viewHolder = HabitViewHolder(itemView)
        viewHolder.onClick(itemClickListener)
        return viewHolder
    }

    override fun onBindViewHolder(holder: HabitViewHolder, position: Int) {
        holder.goaltitle.text = data!![position].goal
        holder.habit.text = data!![position].newHabit
    }

    internal inner class HabitViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var goaltitle: TextView = view.goalTitle
        var habit: TextView = view.dueDate
    }

    fun <T : RecyclerView.ViewHolder> T.onClick(event: (view: View, position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(it, adapterPosition, itemViewType)
        }
        return this
    }

}
package zm.co.goalgetter.planner.configuration

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import zm.co.goalgetter.planner.repository.RealmMigrations

class PlannerApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)

        val configuration = RealmConfiguration.Builder()
                .name("GoalGetter.realm")
                .schemaVersion(2)
                .migration(RealmMigrations())
                .build()

        Realm.setDefaultConfiguration(configuration)
        Realm.getInstance(configuration)
    }

    override fun onTerminate() {
        Realm.getDefaultInstance().close()
        super.onTerminate()
    }

}
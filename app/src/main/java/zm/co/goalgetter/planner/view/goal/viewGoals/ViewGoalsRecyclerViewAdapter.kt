package zm.co.goalgetter.planner.view.goal.viewGoals

import android.support.v7.widget.RecyclerView

import android.view.ViewGroup
import android.widget.TextView
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.model.GoalModel
import android.view.LayoutInflater
import android.view.View
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.goal_list_content.view.*

internal class ViewGoalsRecyclerViewAdapter(data: OrderedRealmCollection<GoalModel>,
                                            val itemClickListener: (View, Int, Int) -> Unit) :
        RealmRecyclerViewAdapter<GoalModel, ViewGoalsRecyclerViewAdapter.GoalViewHolder>(data, true) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.goal_list_content, parent, false)

        val viewHolder = GoalViewHolder(itemView)
        viewHolder.onClick(itemClickListener)
        return viewHolder
    }

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {
        holder.goaltitle.text = data!![position].specific
        holder.dueDate.text = data!![position].timely.toString()
    }

    internal inner class GoalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var goaltitle: TextView
        var dueDate: TextView

        init {
            goaltitle = view.goalTitle
            dueDate = view.dueDate
        }
    }

    fun <T : RecyclerView.ViewHolder> T.onClick(event: (view: View, position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(it, getAdapterPosition(), getItemViewType())
        }
        return this
    }

}
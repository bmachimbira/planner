package zm.co.goalgetter.planner.view.review.addReview

import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.api.ReviewApi
import zm.co.goalgetter.planner.domain.Review
import java.util.*

class AddReviewPresenter(val view: AddReviewContract.View, val habitApi: HabitApi, val reviewApi: ReviewApi) : AddReviewContract.UserActions {
    private var habits = mutableListOf<String>()

    override fun loadAspectsIntoSpinner() {
        val habitList = habitApi.getAllHabits()
        for (habit in habitList) {
            habits.add(habit.goal!!)
        }

        view.loadHabitsIntoSpinner(habits)
    }

    override fun saveReview(habit: Int, performance: String?, highlights: String?, lessons: String?, obstacles: String?, howDoIDoBetter: String?) {
        val selectedGoal = habits[habit]

        val review = Review(
                id = Date().time.toString(),
                goal = selectedGoal,
                performance = performance!!,
                highlights = highlights!!,
                lessons = lessons!!,
                obstacles = obstacles!!,
                howDoIDoBetter = howDoIDoBetter!!
        )
        reviewApi.saveReview(review = review)
        view.endFragment()
    }
}
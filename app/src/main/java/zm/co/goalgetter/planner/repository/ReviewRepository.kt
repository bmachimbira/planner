package zm.co.goalgetter.planner.repository

import io.realm.Realm
import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.mapper.ReviewMapper
import zm.co.goalgetter.planner.model.GoalModel
import zm.co.goalgetter.planner.model.ReviewModel

class ReviewRepository constructor(var realm: Realm, var mapper: ReviewMapper) : DataRepository<Review> {

    fun getAllResults(): RealmResults<ReviewModel> {
        return realm.where(ReviewModel::class.java).findAll()
    }

    override fun getById(id: String): Review {
        return mapper.mapToDomain(realm.where(ReviewModel::class.java).equalTo("id", id).findFirst()!!)
    }

    override fun getAll(): List<Review> {
        return mapper.mapToDomainList(realm.where(ReviewModel::class.java).findAll())
    }

    override fun insert(value: Review) {
        realm.executeTransaction {
            val reviewModel: ReviewModel = realm.createObject(ReviewModel::class.java)
            reviewModel.id = value.id!!
            reviewModel.goal = value.goal!!
            reviewModel.performance = value.performance!!
            reviewModel.highlights = value.highlights!!
            reviewModel.lessons = value.lessons!!
            reviewModel.obstacles = value.obstacles!!
            reviewModel.howDoIDoBetter = value.howDoIDoBetter!!
        }
    }

    override fun update(value: Review) {
        realm.executeTransaction {
            val reviewModel: ReviewModel = realm.createObject(ReviewModel::class.java)
            reviewModel.id = value.id!!
            reviewModel.goal = value.goal!!
            reviewModel.performance = value.performance!!
            reviewModel.highlights = value.highlights!!
            reviewModel.lessons = value.lessons!!
            reviewModel.obstacles = value.obstacles!!
            reviewModel.howDoIDoBetter = value.howDoIDoBetter!!
        }
    }

    override fun delete(id: String) {
        realm.executeTransaction {
            val goal = realm.where(GoalModel::class.java).equalTo("id", id).findAll()
            goal.deleteAllFromRealm()
        }
    }

    override fun deleteAll() {
        realm.executeTransaction {
            realm.deleteAll()
        }
    }

    fun getReviewFromModel(reviewModel: ReviewModel?): Review {
        return mapper.mapToDomain(reviewModel = reviewModel!!)
    }

}
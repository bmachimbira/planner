package zm.co.goalgetter.planner.view.quarterly.addAssessment

interface AddAssessmentContract {
    interface View {
        fun populateCategoriesWithList(categories: List<String>)
        fun endFragment()
    }

    interface UserActions {

        fun saveAssessment(category: Int?, rating: String?, state: String?, costToMe: String?, obstacles: String?, goal: String?, newHabit: String?, resourcesNeeded: String?)
        fun populateCategoryList()
    }
}
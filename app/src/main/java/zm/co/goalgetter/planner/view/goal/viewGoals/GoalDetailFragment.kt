package zm.co.goalgetter.planner.view.goal.viewGoals

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_add_goal.*
import kotlinx.android.synthetic.main.activity_add_goal.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import java.text.SimpleDateFormat
import java.util.*

class  GoalDetailFragment : Fragment() {

    lateinit private var presenter: GoalsListPresenter
    lateinit private var goalId: String
    lateinit private var goal: Goal
    private var editable: Boolean = false
    private var mListener: OnFragmentInteractionListener? = null

    companion object {
        fun newInstance(presenter: GoalsListPresenter, goal: Goal): GoalDetailFragment {
            val fragment = GoalDetailFragment()
            fragment.presenter = presenter
            fragment.goal = goal
            fragment.goalId = goal.id!!
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.activity_add_goal, container, false)

        loadGoalIntoView(view, goal)
        disableEditing(view)

        view.saveGoalButton.setOnClickListener { fabClick(view) }
        view.viewTasksButton.visibility = View.VISIBLE
        view.viewTasksButton.setOnClickListener { loadTasksForGoal() }
        view.timely.visibility = View.GONE

        return view
    }

    private fun loadTasksForGoal() {
        activity.supportFragmentManager.beginTransaction()
                .replace(goalContainerFragment.id, GoalTaskListFragment.newInstance(goalId))
                .addToBackStack("Goal")
                .commit()
    }

    private fun backToHome() {
        activity.supportFragmentManager.beginTransaction()
                .replace(goalContainerFragment.id, GoalListFragment().newInstance())
                .addToBackStack("Goal")
                .commit()
    }

    private fun fabClick(view: View?) {
        if(!editable) {
            enableEditing(view!!)
            return
        }

        val year = timeline.selectedYear
        val month = timeline.selectedMonth
        val day = timeline.selectedDay

        val date = GregorianCalendar(year, month, day).time
        presenter.updateGoal(
                goalId,
                aspect = aspect.selectedItemPosition,
                timely = timely.text.toString(),
                specific = specify.text.toString(),
                relevant = relevance.text.toString(),
                measurable = measure.text.toString(),
                attainable = attain.text.toString(),
                reward = reward.text.toString())
        saveGoalButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_edit))
        disableEditing(view!!)
    }

    private fun disableEditing(view: View) {
        editable = false
        view.specify.isEnabled = false
        view.reward.isEnabled = false
        view.relevance.isEnabled = false
        view.measure.isEnabled = false
        view.attain.isEnabled = false
        view.timeline.isEnabled = false
    }

    private fun enableEditing(view: View) {
        editable = true
        view.specify.isEnabled = true
        view.reward.isEnabled = true
        view.relevance.isEnabled = true
        view.measure.isEnabled = true
        view.attain.isEnabled = true
        view.timeline.isEnabled = true
        view.saveGoalButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_save))
    }

    private fun loadGoalIntoView(view: View, goal: Goal) {
        val date = goal.timely
        val cal = Calendar.getInstance()

        val formatter = SimpleDateFormat("dd-MMM-yyyy")
        val dateFormatted = formatter.parse(date)

        cal.time = dateFormatted
        val month = cal.get(Calendar.MONTH + 1)
        val year = cal.get(Calendar.YEAR)
        val day = cal.get(Calendar.DAY_OF_MONTH)

        view.specify.setText(goal.specific)
        view.measure.setText(goal.measurable)
        view.attain.setText(goal.attainable)
        view.relevance.setText(goal.relevant)
        view.reward.setText(goal.reward)
        view.timeline.setSelectedDate(year, month, day)

        view.aspect.adapter = ArrayAdapter(activity, R.layout.spinner_item, presenter.aspects)
        view.aspect.setSelection(presenter.aspects.indexOf(goal.aspect))
        view.saveGoalButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_edit))
    }

    fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }
}
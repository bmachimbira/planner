package zm.co.goalgetter.planner.view.review.addReview


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_add_review.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.api.ReviewApi
import zm.co.goalgetter.planner.mapper.HabitMapper
import zm.co.goalgetter.planner.mapper.ReviewMapper
import zm.co.goalgetter.planner.repository.HabitRepository
import zm.co.goalgetter.planner.repository.ReviewRepository

class AddReviewFragment : Fragment(), AddReviewContract.View {

    companion object {
        fun newInstance(): Fragment {
            return AddReviewFragment()
        }
    }

    private lateinit var presenter: AddReviewContract.UserActions
    private lateinit var aspects: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = AddReviewPresenter(
                view = this,
                habitApi = HabitApi(
                        habitRepository = HabitRepository(
                                realm = Realm.getDefaultInstance(),
                                mapper = HabitMapper())),
                reviewApi = ReviewApi(
                        reviewRepository = ReviewRepository(
                                realm = Realm.getDefaultInstance(),
                                mapper = ReviewMapper())))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_add_review, container, false)
        aspects = root.category

        presenter.loadAspectsIntoSpinner()
        root.saveReviewButton.setOnClickListener {
            presenter.saveReview(
                    habit = aspects.selectedItemPosition,
                    performance = root.performance!!.text.toString(),
                    highlights = root.highlights!!.text.toString(),
                    lessons = root.lessons!!.text.toString(),
                    obstacles = root.obstacles!!.text.toString(),
                    howDoIDoBetter = root.howDoIDoBetter!!.text.toString())
        }
        return root
    }

    override fun loadHabitsIntoSpinner(habits: List<String>) {
        aspects.adapter = ArrayAdapter<String>(activity, R.layout.spinner_item, habits)
    }

    override fun endFragment() {
        activity.onBackPressed()
    }
}

package zm.co.goalgetter.planner.view.review.viewReviewList

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.model.ReviewModel

interface ViewReviewListContract {
    interface UserActions {

        fun initialiseList()
        fun getItemAtIndex(position: Int)
    }

    interface View {

        fun showDetail(review: Review)
        fun loadReviewsIntoViews(reviews: RealmResults<ReviewModel>)
    }
}
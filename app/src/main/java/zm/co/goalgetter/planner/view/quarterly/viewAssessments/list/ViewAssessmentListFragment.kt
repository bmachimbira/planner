package zm.co.goalgetter.planner.view.quarterly.viewAssessments.list

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_view_assessment_list.*
import kotlinx.android.synthetic.main.fragment_view_assessment_list.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.mapper.HabitMapper
import zm.co.goalgetter.planner.model.HabitModel
import zm.co.goalgetter.planner.repository.HabitRepository
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import zm.co.goalgetter.planner.view.quarterly.addAssessment.AssessmentFragment
import zm.co.goalgetter.planner.view.quarterly.viewAssessments.detail.ViewAssessmentDetailFragment
import zm.co.goalgetter.planner.view.quarterly.viewAssessments.ViewHabitsRecyclerViewAdapter

class ViewAssessmentListFragment : Fragment(), ViewAssessmentListContract.View {

    companion object {
        fun newInstance(): ViewAssessmentListFragment {
            return ViewAssessmentListFragment()
        }
    }

    private var mListener: OnFragmentInteractionListener? = null
    private lateinit var adapter: ViewHabitsRecyclerViewAdapter
    private lateinit var presenter: ViewAssessmentListContract.UserActions
    private lateinit var habitsRecyclerView: RecyclerView
    private lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val habitApi = HabitApi(habitRepository = HabitRepository(realm = Realm.getDefaultInstance(), mapper = HabitMapper()))
        presenter = ViewAssessmentListPresenter(view = this, habitApi = habitApi)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_view_assessment_list, container, false)

        fab = view.addHabit
        habitsRecyclerView = view.habitsRecyclerView

        presenter.initialiseList()
        fab.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_add_circle_black_24dp))

        fab.setOnClickListener { _ ->
            showFragment(fragment = AssessmentFragment.newInstance())
        }
        return view
    }

    private fun showFragment(fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(habitContainer.id, fragment)
                .addToBackStack("Habit")
                .commit()
        fab.hide()
    }


    override fun onResume() {
        super.onResume()
        presenter.initialiseList()
        fab.show()
    }

    override fun setAdapter(habits: RealmResults<HabitModel>) {
        adapter = ViewHabitsRecyclerViewAdapter(habits, itemOnClick)
        habitsRecyclerView.layoutManager = LinearLayoutManager(activity)
        habitsRecyclerView.adapter = adapter
        habitsRecyclerView.setHasFixedSize(true)
    }

    override fun showDetail(habit: Habit) {
        onButtonPressed(false)
        showFragment(fragment = ViewAssessmentDetailFragment.newInstance(habit))
    }

    val itemOnClick: (View, Int, Int) -> Unit = { view, position, type ->
        presenter.getItemAtIndex(position)
    }

    private fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }



}

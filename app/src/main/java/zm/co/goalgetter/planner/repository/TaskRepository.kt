package zm.co.goalgetter.planner.repository

import io.realm.Realm
import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.mapper.TaskMapper
import zm.co.goalgetter.planner.model.TaskModel

class TaskRepository constructor(var realm: Realm, var mapper: TaskMapper) : DataRepository<Task> {
    override fun getById(id: String): Task {
        return mapper.mapToDomain(realm.where(TaskModel::class.java).equalTo("id", id).findFirst()!!)
    }

    override fun getAll(): List<Task> {
        return mapper.mapToDomainList(realm.where(TaskModel::class.java).findAll())
    }

    override fun insert(value: Task) {
        realm.executeTransaction {
            var task: TaskModel = realm.createObject(TaskModel::class.java)
            task.goalId = value.goalId
            task.title = value.title
            task.due = value.due
            task.description = value.description
        }
    }

    override fun update(value: Task) {
        realm.executeTransaction {
            var task: TaskModel = realm.createObject(TaskModel::class.java)
            task.goalId = value.goalId
            task.title = value.title
            task.due = value.due
            task.description = value.description
        }
    }

    override fun delete(id: String) {
        realm.executeTransaction {
            var task = realm.where(TaskModel::class.java).equalTo("specific", id).findAll()
        }
    }

    override fun deleteAll() {
        realm.executeTransaction {
            realm.deleteAll()
        }
    }

    fun getAllTasks(): RealmResults<TaskModel> {
       return realm.where(TaskModel::class.java).findAll()
    }

    fun getTasksById(goalId: String): RealmResults<TaskModel> {
        return realm.where(TaskModel::class.java).equalTo("goalId", goalId).findAll()
    }

    fun getTaskFromModel(taskModel: TaskModel?): Task {
        return mapper.mapToDomain(taskModel!!)
    }
}
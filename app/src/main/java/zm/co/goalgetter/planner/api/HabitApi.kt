package zm.co.goalgetter.planner.api

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.model.HabitModel
import zm.co.goalgetter.planner.repository.HabitRepository

class HabitApi constructor(private var habitRepository: HabitRepository){

    fun saveHabit(habit: Habit){
        habitRepository.insert(habit)
    }

    fun getAllHabits(): List<Habit> {
        return habitRepository.getAll()
    }

    fun getAllResults(): RealmResults<HabitModel> {
        return habitRepository.getAllResults()
    }

    fun getHabit(habitId: String): Habit {
        return habitRepository.getById(habitId)
    }

    fun getHabitFromModel(habitModel: HabitModel?): Habit {
        return habitRepository.getHabitFromModel(habitModel)
    }

    fun updateGoal(habit: Habit) {
        habitRepository.update(habit)
    }

}
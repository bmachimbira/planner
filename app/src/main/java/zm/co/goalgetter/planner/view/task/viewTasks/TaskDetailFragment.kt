package zm.co.goalgetter.planner.view.goal.viewGoals

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.content_add_task.*
import kotlinx.android.synthetic.main.content_add_task.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import zm.co.goalgetter.planner.view.task.viewTasks.TaskListContract
import java.util.*

class TaskDetailFragment : Fragment() {

    lateinit private var presenter: TaskListContract.Presenter
    lateinit private var goalId: String
    lateinit private var task: Task
    private var editable: Boolean = false
    private var mListener: OnFragmentInteractionListener? = null


    fun newInstance(presenter: TaskListContract.Presenter, task: Task): TaskDetailFragment {
        val fragment = TaskDetailFragment()
        fragment.presenter = presenter
        fragment.task = task
        fragment.goalId = task.goalId!!
        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.activity_add_task, container, false)

        loadTaskIntoView(view, task)
        disableEditing(view)

        view.saveTaskButton.setOnClickListener {fabClick(view) }
        view.goalListSpinner.visibility = View.GONE
        return view
    }

    private fun fabClick(view: View?) {
        if (!editable) {
            enableEditing(view!!)
            return
        }
        val year = view!!.taskDueTimeline.selectedYear
        val month = view.taskDueTimeline.selectedMonth
        val day = view.taskDueTimeline.selectedDay

        val date = GregorianCalendar(year, month, day).time

        presenter.updateTask(
                task.goalId!!,
                taskTitleInput.text.toString(),
                descriptionInput.text.toString(),
                date)
        saveTaskButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_edit))
        disableEditing(view)
    }

    private fun disableEditing(view: View) {
        editable = false
        view.taskTitleInput.isEnabled = false
        view.descriptionInput.isEnabled = false
        view.taskDueTimeline.isEnabled = false
    }

    private fun enableEditing(view: View) {
        editable = true
        view.taskTitleInput.isEnabled = true
        view.descriptionInput.isEnabled = true
        view.taskDueTimeline.isEnabled = true
        view.saveTaskButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_save))
    }

    private fun loadTaskIntoView(view: View, task: Task) {
        val date = task.due
        val cal = Calendar.getInstance()
        cal.time = date
        val month = cal.get(Calendar.MONTH + 1)
        val year = cal.get(Calendar.YEAR)
        val day = cal.get(Calendar.DAY_OF_MONTH)

        view.taskTitleInput.setText(task.title)
        view.descriptionInput.setText(task.description)
        view.taskDueTimeline.setSelectedDate(year, month, day)

        view.saveTaskButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_edit))
    }

    fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}
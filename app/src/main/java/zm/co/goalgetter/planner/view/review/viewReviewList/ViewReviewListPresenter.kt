package zm.co.goalgetter.planner.view.review.viewReviewList

import io.realm.RealmResults
import zm.co.goalgetter.planner.api.ReviewApi
import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.model.ReviewModel

class ViewReviewListPresenter(val reviewApi: ReviewApi, val view: ViewReviewListContract.View) : ViewReviewListContract.UserActions{

    private lateinit var reviews: RealmResults<ReviewModel>


    override fun initialiseList() {
        this.reviews = reviewApi.getAllResults()
        view.loadReviewsIntoViews(reviews = reviews)
    }

    override fun getItemAtIndex(position: Int) {
        val review = reviewApi.getReviewFromModel(reviews[position])
        view.showDetail(review = review)
    }

}
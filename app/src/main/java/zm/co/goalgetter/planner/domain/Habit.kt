package zm.co.goalgetter.planner.domain

class Habit constructor() {
    var id: String? = null
    var category: String? = null
    var rating: String? = null
    var state: String? = null
    var costToMe: String? = null
    var obstacles: String? = null
    var goal: String? = null
    var newHabit: String? = null
    var resourcesNeeded: String? = null


    constructor(id: String?,
            category: String? ,
            rating: String?,
            state: String?,
            costToMe: String? ,
            obstacles: String?,
            goal: String? ,
            newHabit: String? ,
            resourcesNeeded: String?) : this() {
        this.id = id
        this.category = category
        this.rating = rating
        this.state = state
        this.costToMe = costToMe
        this.obstacles = obstacles
        this.goal = goal
        this.newHabit = newHabit
        this.resourcesNeeded = resourcesNeeded
    }
}
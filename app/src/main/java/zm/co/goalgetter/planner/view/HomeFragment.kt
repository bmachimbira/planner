package zm.co.goalgetter.planner.view

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.view.goal.addGoal.AddGoalFragment
import zm.co.goalgetter.planner.view.task.addTask.AddTaskFragment
import zm.co.goalgetter.planner.view.goal.viewGoals.GoalListFragment
import zm.co.goalgetter.planner.view.task.viewTasks.TaskListFragment


class HomeFragment : Fragment(), OnFragmentInteractionListener{

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var mViewPager: ViewPager? = null

    fun newInstance(): Fragment{
        return HomeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.activity_home, container, false)

        mSectionsPagerAdapter = SectionsPagerAdapter(activity.supportFragmentManager)

        mViewPager = view.container as ViewPager
        mViewPager!!.adapter = mSectionsPagerAdapter

        val tabLayout = view.tabs as TabLayout

        mViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(mViewPager))

        view.fab.setOnClickListener { _ ->
            val pager = mViewPager
            val index = pager!!.currentItem


            var addIntent = Intent(activity, AddGoalFragment::class.java)
            if (index != 0) {
                addIntent = Intent(activity, AddTaskFragment::class.java)
            }

            startActivity(addIntent)
        }

        return view
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            if (position == 0) {
                return GoalListFragment().newInstance()
            }
            return TaskListFragment().newInstance()
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 2
        }
    }

    override fun onFragmentInteraction(fabVisible: Boolean) {
        if(!fabVisible){
            fab.visibility = View.GONE
            return
        }
        fab.visibility = View.VISIBLE
    }
}

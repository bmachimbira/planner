package zm.co.goalgetter.planner.mapper

import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.model.TaskModel

class TaskMapper{

    fun mapToDomain(task: TaskModel): Task {
        return Task(goalId = task.goalId!!,
                    title = task.title!!,
                    due = task.due!!,
                    description = task.description!!
                )
    }

    fun mapToDomainList(taskList: List<TaskModel>): List<Task>{
        var tasks = mutableListOf<Task>()
        taskList.forEach{ task->
            tasks.add(mapToDomain(task))
        }
        return tasks
    }

}
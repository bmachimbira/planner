package zm.co.goalgetter.planner.model

import io.realm.RealmObject

open class ReviewModel : RealmObject() {
    var id: String? = null
    var goal: String? = null
    var performance: String? = null
    var highlights: String? = null
    var lessons: String? = null
    var obstacles: String? = null
    var howDoIDoBetter: String? = null
}
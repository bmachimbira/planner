package zm.co.goalgetter.planner.view.self

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.getColor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.RadarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import kotlinx.android.synthetic.main.fragment_self.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.common.getWheelOfLifeValues
import java.util.*


class SelfFragment : Fragment(), SelfContract.View {

    companion object {
        fun newInstance(): Fragment {
            return SelfFragment()
        }
    }

    private lateinit var mChart: RadarChart

    private var mActivities = mutableListOf<String>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_self, container, false)

        val generator = Random()
        for (number in 1..8){
            val randomIndex = generator.nextInt(getWheelOfLifeValues().size)
            mActivities.add(getWheelOfLifeValues()[randomIndex])
        }

        mChart = view.chart1
        mChart.setBackgroundColor(getColor(activity, R.color.goalGetterPrimary))
        mChart.description.isEnabled = false
        mChart.webLineWidth = 1f
        mChart.webColor = Color.LTGRAY
        mChart.webLineWidthInner = 1f
        mChart.webColorInner = Color.LTGRAY
        mChart.webAlpha = 100

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        val mv = RadarMarkerView(activity, R.layout.radar_markerview)
        mv.chartView = mChart // For bounds control
        mChart.marker = mv // Set the marker to the chart

        setData()
        mChart.animateXY(
                1400, 1400,
                Easing.EasingOption.EaseInOutQuad,
                Easing.EasingOption.EaseInOutQuad)

        val xAxis = mChart.xAxis
        xAxis.textSize = 12f
        xAxis.yOffset = 0f
        xAxis.xOffset = 0f
        xAxis.valueFormatter = IAxisValueFormatter { value, _ -> mActivities[value.toInt() % mActivities.size] }
        xAxis.textColor = Color.WHITE

        val yAxis = mChart.yAxis
        yAxis.setLabelCount(10, false)
        yAxis.textSize = 12f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 80f
        yAxis.setDrawLabels(false)

        val l = mChart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.xEntrySpace = 20f
        l.yEntrySpace = 5f
        l.textColor = Color.WHITE
        return view
    }

    private fun setData() {

        val mult = 80f
        val min = 20f
        val cnt = mActivities.size

        val entries1 = ArrayList<RadarEntry>()
        val entries2 = ArrayList<RadarEntry>()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (i in 0 until cnt) {
            val val1 = (Math.random() * mult).toFloat() + min
            entries1.add(RadarEntry(val1))

            val val2 = (Math.random() * mult).toFloat() + min
            entries2.add(RadarEntry(val2))
        }

        val set1 = RadarDataSet(entries1, "Last Quarter")
        set1.color = Color.rgb(103, 110, 129)
        set1.fillColor = Color.rgb(103, 110, 129)
        set1.setDrawFilled(true)
        set1.fillAlpha = 180
        set1.lineWidth = 2f
        set1.isDrawHighlightCircleEnabled = true
        set1.setDrawHighlightIndicators(false)

        val set2 = RadarDataSet(entries2, "This Quarter")
        set2.color = Color.rgb(121, 162, 175)
        set2.fillColor = Color.rgb(121, 162, 175)
        set2.setDrawFilled(true)
        set2.fillAlpha = 180
        set2.lineWidth = 2f
        set2.isDrawHighlightCircleEnabled = true
        set2.setDrawHighlightIndicators(false)

        val sets = ArrayList<IRadarDataSet>()
        sets.add(set1)
        sets.add(set2)

        val data = RadarData(sets)
        data.setValueTextSize(8f)
        data.setDrawValues(false)
        data.setValueTextColor(Color.WHITE)

        mChart.data = data
        mChart.invalidate()
    }
}

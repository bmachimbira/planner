package zm.co.goalgetter.planner.api

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.model.GoalModel
import zm.co.goalgetter.planner.repository.GoalRepository

class GoalApi constructor(var goalRepository: GoalRepository){

    fun saveGoal(goal: Goal){
        goalRepository.insert(goal)
    }

    fun getAllResults():RealmResults<GoalModel>{
        return goalRepository.getAllResults()
    }

    fun getGoal(goalId: String): Goal {
        return goalRepository.getById(goalId)
    }

    fun getGoalFromModel(goalModel: GoalModel?): Goal {
        return goalRepository.getGoalFromModel(goalModel)
    }

    fun updateGoal(goal: Goal) {
        goalRepository.update(goal)
    }

}
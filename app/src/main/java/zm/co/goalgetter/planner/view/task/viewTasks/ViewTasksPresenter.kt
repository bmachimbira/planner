package zm.co.goalgetter.planner.view.task.viewTasks

import io.realm.RealmResults
import zm.co.goalgetter.planner.api.TaskApi
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.model.TaskModel
import java.util.*

class ViewTasksPresenter (var view: TaskListContract.View, var taskApi: TaskApi) :
        TaskListContract.Presenter{


    lateinit private var tasks: RealmResults<TaskModel>

    override fun initialiseList() {
        this.tasks = taskApi.getAllTasks()
        view.loadTasksIntoAdapter(tasks)
    }

    override fun getItemAtIndex(position: Int) {
        val task = taskApi.getTaskFromModel(tasks[position])
        view.showDetail(task = task)
    }

    override fun updateTask(goalSelected: String, taskTitle: String, description: String, dueDate: Date?) {
        var task = Task(
                goalId = goalSelected,
                title = taskTitle,
                due = dueDate!!,
                description = description
        )
        taskApi.saveTask(task)
    }

    override fun initialiseListForGoal(goalId: String) {
        this.tasks = taskApi.getTasksByGoal(goalId)
        view.loadTasksIntoAdapter(tasks)
    }
}
package zm.co.goalgetter.planner.view

interface OnFragmentInteractionListener {
    fun onFragmentInteraction(fabVisible: Boolean)
}
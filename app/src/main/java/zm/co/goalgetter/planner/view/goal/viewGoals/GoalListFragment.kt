package zm.co.goalgetter.planner.view.goal.viewGoals

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.activity_main.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.GoalApi
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.model.GoalModel
import zm.co.goalgetter.planner.repository.GoalRepository
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import zm.co.goalgetter.planner.view.goal.addGoal.AddGoalFragment

class GoalListFragment : Fragment(), GoalListContract.View {

    private lateinit var presenter: GoalsListPresenter
    private lateinit var adapter: ViewGoalsRecyclerViewAdapter
    private lateinit var fab: FloatingActionButton
    private var mListener: OnFragmentInteractionListener? = null

    fun newInstance(): Fragment {
        return GoalListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.activity_main, container, false)
        presenter = GoalsListPresenter(this, GoalApi(GoalRepository(Realm.getDefaultInstance(), GoalMapper())))

        fab = view.fab
        fab.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_add_circle_black_24dp))

        fab.setOnClickListener { _ ->
            showFragment(fragment = AddGoalFragment.newInstance())
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.initialiseList()
        fab.show()
    }

    override fun setAdapter(goals: RealmResults<GoalModel>) {
        adapter = ViewGoalsRecyclerViewAdapter(goals, itemOnClick)
        goalsRecyclerView.layoutManager = LinearLayoutManager(activity)
        goalsRecyclerView.adapter = adapter
        goalsRecyclerView.setHasFixedSize(true)
    }

    override fun showDetail(goal: Goal) {
        onButtonPressed(false)
        showFragment(fragment = GoalDetailFragment.newInstance(presenter, goal))
    }

    private fun showFragment(fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(goalContainerFragment.id, fragment)
                .addToBackStack("Goal")
                .commit()
        fab.hide()
    }

    val itemOnClick: (View, Int, Int) -> Unit = { view, position, type ->
        presenter.getItemAtIndex(position)
    }

    private fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}

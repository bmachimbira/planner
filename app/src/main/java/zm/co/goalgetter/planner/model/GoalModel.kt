package zm.co.goalgetter.planner.model

import io.realm.RealmObject

open class GoalModel : RealmObject() {
    var id: String? = null
    var aspect: String? = null
    var specific: String? = null
    var measurable: String? = null
    var attainable: String? = null
    var relevant: String? = null
    var timely: String? = null
    var reward: String? = null
}

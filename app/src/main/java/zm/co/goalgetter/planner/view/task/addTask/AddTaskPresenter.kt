package zm.co.goalgetter.planner.view.task.addTask

import zm.co.goalgetter.planner.api.TaskApi
import zm.co.goalgetter.planner.domain.Task
import java.util.*

class AddTaskPresenter(var view: AddTaskContract.View,  var taskApi: TaskApi) : AddTaskContract.Presenter{
    private lateinit var goals: Map<String, String>

    override fun saveTask(goalSelected: String, taskTitle: String, description: String, dueDate: Date?) {
        val goal = getKeyFromValue(goals, goalSelected)
        var task = Task(
                goalId = goal!!,
                title = taskTitle,
                due = dueDate!!,
                description = description
        )
        taskApi.saveTask(task)
        view.endFragment()
    }


    override fun getGoals(){
        this.goals = taskApi.getAllGoals()
        var goalTitles = mutableListOf<String>()

        for (title in goals){
            goalTitles.add(title.value)
        }
        view.populateGoalList(goalTitles)
    }

    fun getKeyFromValue(hm: Map<String, String>, value: String): String? {
        for (o in hm.keys) {
            if (hm[o] == value) {
                return o
            }
        }
        return null
    }
}
package zm.co.goalgetter.planner.view.task.addTask

import java.util.*

interface AddTaskContract {

    interface View {
        fun populateGoalList(goals: List<String>)
        fun endFragment()
    }

    interface Presenter {
        fun getGoals()
        fun saveTask(goalSelected: String, taskTitle: String, description: String, dueDate: Date?)
    }
}
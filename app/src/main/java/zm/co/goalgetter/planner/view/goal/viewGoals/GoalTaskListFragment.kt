package zm.co.goalgetter.planner.view.goal.viewGoals

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_view_tasks.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.TaskApi
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.mapper.TaskMapper
import zm.co.goalgetter.planner.model.TaskModel
import zm.co.goalgetter.planner.repository.GoalRepository
import zm.co.goalgetter.planner.repository.TaskRepository
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import zm.co.goalgetter.planner.view.task.addTask.AddTaskFragment
import zm.co.goalgetter.planner.view.task.viewTasks.TaskListContract
import zm.co.goalgetter.planner.view.task.viewTasks.ViewTasksPresenter
import zm.co.goalgetter.planner.view.task.viewTasks.ViewTasksRecyclerViewAdapter

class GoalTaskListFragment : Fragment(), TaskListContract.View {
    lateinit private var presenter: TaskListContract.Presenter
    lateinit private var adapter: ViewTasksRecyclerViewAdapter
    lateinit private var addTaskFab: FloatingActionButton
    lateinit private var tasksRecyclerView: RecyclerView
    lateinit private var taskContainer: FrameLayout
    private var mListener: OnFragmentInteractionListener? = null
    lateinit private var goalId: String

    companion object {
        fun newInstance(goalId: String): Fragment {
            val fragment = GoalTaskListFragment()
            fragment.goalId = goalId
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.activity_view_tasks, container, false)

        val realm = Realm.getDefaultInstance()
        this.addTaskFab = view.addTaskFab
        this.tasksRecyclerView = view.tasksRecyclerView
        this.taskContainer = view.taskContainer
        presenter = ViewTasksPresenter(
                this,
                TaskApi(TaskRepository(realm, TaskMapper()), GoalRepository(realm, GoalMapper())))
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.initialiseListForGoal(goalId)
        onButtonPressed(true)
        addTaskFab.setOnClickListener {
            showFragment(AddTaskFragment.newInstance())
        }
        addTaskFab.show()
    }

    override fun showDetail(task: Task) {
        onButtonPressed(false)
        val goalFragment = TaskDetailFragment().newInstance(presenter, task)
        showFragment(goalFragment)
    }

    private fun showFragment(goalFragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(taskContainer.id, goalFragment)
                .commit()
        addTaskFab.hide()
    }

    override fun loadTasksIntoAdapter(tasks: RealmResults<TaskModel>) {
        adapter = ViewTasksRecyclerViewAdapter(tasks, itemOnClick)
        tasksRecyclerView.layoutManager = LinearLayoutManager(activity)
        tasksRecyclerView.adapter = adapter
        tasksRecyclerView.setHasFixedSize(true)
    }

    val itemOnClick: (View, Int, Int) -> Unit = { view, position, type ->
        presenter.getItemAtIndex(position)
    }

    private fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}

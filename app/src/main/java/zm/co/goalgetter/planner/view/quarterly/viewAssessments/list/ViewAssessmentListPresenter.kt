package zm.co.goalgetter.planner.view.quarterly.viewAssessments.list

import io.realm.RealmResults
import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.model.HabitModel

class ViewAssessmentListPresenter(val view: ViewAssessmentListContract.View, val habitApi: HabitApi) : ViewAssessmentListContract.UserActions {
    lateinit private var habits: RealmResults<HabitModel>

    override fun initialiseList() {
        habits = habitApi.getAllResults()
        view.setAdapter(habits)
    }

    override fun getItemAtIndex(position: Int) {
        val habit = habitApi.getHabitFromModel(habits[position])
        view.showDetail(habit = habit)
    }
}
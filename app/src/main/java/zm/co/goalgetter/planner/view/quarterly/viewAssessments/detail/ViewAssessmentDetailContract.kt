package zm.co.goalgetter.planner.view.quarterly.viewAssessments.detail

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.model.HabitModel

interface ViewAssessmentDetailContract {
    interface UserActions {
        fun populateList()

    }

    interface View {
        fun setAdapter(habits: List<String>)

    }
}

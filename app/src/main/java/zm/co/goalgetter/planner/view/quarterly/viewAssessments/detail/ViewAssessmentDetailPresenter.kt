package zm.co.goalgetter.planner.view.quarterly.viewAssessments.detail

import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.common.getWheelOfLifeValues
import zm.co.goalgetter.planner.domain.Habit

class ViewAssessmentDetailPresenter(val view: ViewAssessmentDetailContract.View, val habitApi: HabitApi) : ViewAssessmentDetailContract.UserActions {
    lateinit private var habits: List<Habit>

    private var categories = getWheelOfLifeValues()

    override fun populateList() {
        habits = habitApi.getAllHabits()
        view.setAdapter(categories)
    }
}
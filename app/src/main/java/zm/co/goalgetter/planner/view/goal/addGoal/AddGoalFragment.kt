package zm.co.goalgetter.planner.view.goal.addGoal

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_add_goal.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.GoalApi
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.repository.GoalRepository
import java.text.SimpleDateFormat
import java.util.*

class AddGoalFragment : Fragment(), AddGoalContract.View {

    companion object {
        fun newInstance(): Fragment{
            return AddGoalFragment()
        }
    }

    lateinit private var presenter: AddGoalPresenter
    private lateinit var aspectSpinner: Spinner

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.activity_add_goal, container, false)

        this.presenter = AddGoalPresenter(this, GoalApi(GoalRepository(Realm.getDefaultInstance(), GoalMapper())))
        this.aspectSpinner = view.aspect
        view.viewTasksButton.visibility = View.GONE
        view.saveGoalButton.setImageDrawable(ContextCompat.getDrawable(activity, android.R.drawable.ic_menu_edit))
        view.saveGoalButton.setOnClickListener {
            saveGoal(view)
        }

        presenter.loadAspectsIntoSpinner()
        return view
    }

    override fun loadAspects(aspects: List<String>) {
        val adapter = ArrayAdapter(activity, R.layout.spinner_item, aspects)
        this.aspectSpinner.adapter = adapter
    }

    private fun saveGoal(view: View) {
        val year = view.timeline.selectedYear
        val month = view.timeline.selectedMonth
        val day = view.timeline.selectedDay

        val date = GregorianCalendar(year, month, day).time
        val formatter = SimpleDateFormat("dd-MMM-yyyy")
        val dateFormatted = formatter.format(date.time)
        presenter.addGoal(
                aspect = view.aspect.selectedItemPosition,
                timely = dateFormatted,
                specific = view.specify.text.toString(),
                measurable = view.measure.text.toString(),
                attainable = view.attain.text.toString(),
                relevant = view.relevance.text.toString(),
                reward = view.reward.text.toString())
    }

    override fun endFragment() {
        activity.onBackPressed()
    }
}

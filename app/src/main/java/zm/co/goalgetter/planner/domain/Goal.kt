package zm.co.goalgetter.planner.domain

class Goal constructor() {
    var id: String? = null
    var aspect: String? = null
    var specific: String? = null
    var measurable: String? = null
    var attainable: String? = null
    var relevant: String? = null
    var timely: String? = null
    var reward: String? = null

    constructor(id: String,
                specific: String,
                measurable: String,
                aspect: String,
                attainable: String,
                relevant: String,
                timely: String,
                reward: String) : this() {
        this.id = id
        this.specific = specific
        this.aspect = aspect
        this.measurable = measurable
        this.attainable = attainable
        this.relevant = relevant
        this.timely = timely
        this.reward = reward
    }
}
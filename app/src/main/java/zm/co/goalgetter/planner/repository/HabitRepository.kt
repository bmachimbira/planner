package zm.co.goalgetter.planner.repository

import io.realm.Realm
import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.mapper.HabitMapper
import zm.co.goalgetter.planner.model.HabitModel

class HabitRepository constructor(var realm: Realm, var mapper: HabitMapper) : DataRepository<Habit> {

    fun getAllResults(): RealmResults<HabitModel> {
        return realm.where(HabitModel::class.java).findAll()
    }

    override fun getById(id: String): Habit {
        return mapper.mapToDomain(realm.where(HabitModel::class.java).equalTo("id", id).findFirst()!!)
    }

    override fun getAll(): List<Habit> {
        return mapper.mapToDomainList(realm.where(HabitModel::class.java).findAll())
    }

    override fun insert(value: Habit) {
        realm.executeTransaction {
            val habitModel: HabitModel = realm.createObject(HabitModel::class.java)
            habitModel.id = value.id!!
            habitModel.category = value.category
            habitModel.rating = value.rating
            habitModel. state = value.state
            habitModel. costToMe = value.costToMe
            habitModel. obstacles = value.obstacles
            habitModel.goal = value.goal
            habitModel.newHabit = value.newHabit
            habitModel.resourcesNeeded = value.resourcesNeeded
           
        }
    }

    override fun update(value: Habit) {
        realm.executeTransaction {
            val habitModel: HabitModel = realm.createObject(HabitModel::class.java)
            habitModel.id = value.id!!
            habitModel.category = value.category
            habitModel.rating = value.rating
            habitModel. state = value.state
            habitModel. costToMe = value.costToMe
            habitModel. obstacles = value.obstacles
            habitModel.goal = value.goal
            habitModel.newHabit = value.newHabit
            habitModel.resourcesNeeded = value.resourcesNeeded
        }
    }

    override fun delete(id: String) {
        realm.executeTransaction {
            val goal = realm.where(HabitModel::class.java).equalTo("id", id).findAll()
            goal.deleteAllFromRealm()
        }
    }

    override fun deleteAll() {
        realm.executeTransaction {
            realm.deleteAll()
        }
    }

    fun getHabitFromModel(habitModel: HabitModel?): Habit {
        return mapper.mapToDomain(habitModel!!)
    }
}
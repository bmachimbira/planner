package zm.co.goalgetter.planner.view.goal.addGoal

import zm.co.goalgetter.planner.api.GoalApi
import zm.co.goalgetter.planner.common.getWheelOfLifeValues
import zm.co.goalgetter.planner.domain.Goal
import java.util.*

class AddGoalPresenter constructor(var view: AddGoalFragment, var goalApi: GoalApi): AddGoalContract.Presenter{
    private val aspects = getWheelOfLifeValues()

    override fun loadAspectsIntoSpinner(){
        view.loadAspects(aspects = aspects)
    }

    override fun addGoal(aspect: Int, timely: String, specific: String, measurable: String, attainable: String, relevant: String, reward: String) {
        val id = Date().time.toString()

        val aspectSelected = aspects[aspect]
        val goal = Goal(
                id = id,
                aspect = aspectSelected,
                specific = specific,
                reward = reward,
                measurable = measurable,
                attainable = attainable,
                relevant = relevant,
                timely = timely)
        goalApi.saveGoal(goal)
        view.endFragment()
    }
}
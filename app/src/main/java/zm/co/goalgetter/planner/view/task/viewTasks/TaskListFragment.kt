package zm.co.goalgetter.planner.view.task.viewTasks

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_view_tasks.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.TaskApi
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.mapper.TaskMapper
import zm.co.goalgetter.planner.model.TaskModel
import zm.co.goalgetter.planner.repository.GoalRepository
import zm.co.goalgetter.planner.repository.TaskRepository
import zm.co.goalgetter.planner.view.OnFragmentInteractionListener
import zm.co.goalgetter.planner.view.goal.viewGoals.TaskDetailFragment

class TaskListFragment : Fragment(), TaskListContract.View {
    lateinit private var presenter: TaskListContract.Presenter
    lateinit private var adapter: ViewTasksRecyclerViewAdapter
    private var mListener: OnFragmentInteractionListener? = null


    fun newInstance(): Fragment {
        return TaskListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.activity_view_tasks, container, false)

        val realm = Realm.getDefaultInstance()
        presenter = ViewTasksPresenter(
                this,
                TaskApi(TaskRepository(realm, TaskMapper()), GoalRepository(realm, GoalMapper())))
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.initialiseList()
        onButtonPressed(true)
    }

    override fun showDetail(task: Task) {
        onButtonPressed(false)
        val goalFragment = TaskDetailFragment().newInstance(presenter, task)
        activity.supportFragmentManager.beginTransaction()
                .replace(taskContainer.id, goalFragment)
                .addToBackStack("Task")
                .commit()    }

    override fun loadTasksIntoAdapter(tasks: RealmResults<TaskModel>) {
        adapter = ViewTasksRecyclerViewAdapter(tasks, itemOnClick)
        tasksRecyclerView.layoutManager = LinearLayoutManager(activity)
        tasksRecyclerView.adapter = adapter
        tasksRecyclerView.setHasFixedSize(true)
    }

    val itemOnClick: (View, Int, Int) -> Unit = { view, position, type ->
        presenter.getItemAtIndex(position)
    }

    fun onButtonPressed(fabVisible: Boolean) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(fabVisible)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}

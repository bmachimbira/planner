package zm.co.goalgetter.planner.repository

interface DataRepository<T>{
    fun getById(id: String): T
    fun getAll(): List<T>
    fun insert(value: T)
    fun update(value: T)
    fun delete(id: String)
    fun deleteAll()
}
package zm.co.goalgetter.planner.view.self

import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.radar_markerview.view.*
import java.text.DecimalFormat


class RadarMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

    private val format = DecimalFormat("##0")

    override fun refreshContent(e: Entry, highlight: Highlight) {
        tvContent.text = format.format(e.y) + " %"

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height - 10).toFloat())
    }
}
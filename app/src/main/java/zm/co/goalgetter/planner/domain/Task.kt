package zm.co.goalgetter.planner.domain

import java.util.*

class Task constructor(){
    var goalId:String? = null
    var title:String? = null
    var due: Date? = null
    var description: String? = null

    constructor(goalId: String, title:String, due: Date, description: String): this(){
        this.goalId = goalId
        this.title = title
        this.due = due
        this.description = description
    }
}
package zm.co.goalgetter.planner.view

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_navigation.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.view.quarterly.viewAssessments.list.ViewAssessmentListFragment
import zm.co.goalgetter.planner.view.review.viewReviewList.ViewReviewListFragment
import zm.co.goalgetter.planner.view.self.SelfFragment
import zm.co.goalgetter.planner.view.goal.viewGoals.GoalListFragment
import zm.co.goalgetter.planner.view.task.viewTasks.TaskListFragment

class NavigationActivity : AppCompatActivity(), OnFragmentInteractionListener {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
               showFragment(fragment = GoalListFragment().newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_goals -> {
                showFragment(fragment = TaskListFragment().newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_habits -> {
                showFragment(fragment = ViewAssessmentListFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_journal -> {
                showFragment(fragment = ViewReviewListFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_review -> {
                showFragment(fragment = SelfFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        showFragment(fragment = GoalListFragment().newInstance())
    }

    private fun showFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commit()
    }

    override fun onFragmentInteraction(fabVisible: Boolean) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

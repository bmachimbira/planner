package zm.co.goalgetter.planner.view.quarterly.viewAssessments.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_assessment.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.domain.Habit
import zm.co.goalgetter.planner.mapper.HabitMapper
import zm.co.goalgetter.planner.model.HabitModel
import zm.co.goalgetter.planner.repository.HabitRepository


class ViewAssessmentDetailFragment : Fragment(), ViewAssessmentDetailContract.View {

    companion object {
        fun newInstance(habit: Habit): ViewAssessmentDetailFragment {
            val fragment = ViewAssessmentDetailFragment()
            fragment.habit = habit
            return fragment
        }
    }

    private lateinit var presenter: ViewAssessmentDetailContract.UserActions
    private lateinit var categorySpinner: Spinner
    private lateinit var habit: Habit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val habitApi = HabitApi(habitRepository = HabitRepository(realm = Realm.getDefaultInstance(), mapper = HabitMapper()))
        presenter = ViewAssessmentDetailPresenter(view = this, habitApi = habitApi)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_assessment, container, false)
        this.categorySpinner = root.category
        root.currentRating.text = SpannableStringBuilder(habit.rating)
        root.currentState.text = SpannableStringBuilder(habit.state)
        root.currentCost.text = SpannableStringBuilder(habit.costToMe)
        root.obstacles.text = SpannableStringBuilder(habit.obstacles)
        root.goal.text = SpannableStringBuilder(habit.goal)
        root.newHabit.text = SpannableStringBuilder(habit.newHabit)
        root.resourcesNeeded.text = SpannableStringBuilder(habit.resourcesNeeded)

        presenter.populateList()
        return root
    }

    override fun setAdapter(categories: List<String>) {
        val adapter = ArrayAdapter<String>(activity, R.layout.spinner_item, categories)
        categorySpinner.adapter = adapter
        categorySpinner.setSelection(categories.indexOf(habit.category))
    }
}

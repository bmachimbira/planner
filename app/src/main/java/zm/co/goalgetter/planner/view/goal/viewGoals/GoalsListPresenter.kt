package zm.co.goalgetter.planner.view.goal.viewGoals

import io.realm.RealmResults
import zm.co.goalgetter.planner.api.GoalApi
import zm.co.goalgetter.planner.common.getWheelOfLifeValues
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.model.GoalModel

class GoalsListPresenter constructor(var view: GoalListContract.View, private var goalApi: GoalApi) : GoalListContract.Presenter {
    lateinit private var goals: RealmResults<GoalModel>
    val aspects = getWheelOfLifeValues()


    override fun initialiseList() {
        goals = goalApi.getAllResults()
        view.setAdapter(goals)
    }

    override fun loadGoal(goalId: String): Goal {
        return goalApi.getGoal(goalId)
    }

    override fun getItemAtIndex(position: Int) {
        val goal = goalApi.getGoalFromModel(goals[position])
        view.showDetail(goal = goal)
    }

    override fun updateGoal(goalId: String, aspect: Int, timely: String, specific: String, relevant: String, measurable: String, attainable: String, reward: String) {
        val aspectSelected = aspects[aspect]
        val goal = Goal(goalId, aspect = aspectSelected, specific = specific, relevant = relevant, measurable = measurable, attainable = attainable, timely = timely, reward = reward)
        goalApi.updateGoal(goal)
    }

}

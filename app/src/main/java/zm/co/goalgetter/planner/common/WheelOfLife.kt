package zm.co.goalgetter.planner.common

enum class WheelOfLife(val aspect: String) {
    Career("Career"),
    PhysicalEnvironment("Physical environment"),
    PersonalGrowth("Personal growth"),
    Money("Money"),
    Health("Health"),
    FunAndRecreation("Fun and recreation"),
    SignificantOther("Significant other / Romance"),
    FamilyAndFriends("Family and friends"),
    School("School"),
    Business("Business"),
    Spirituality("Spirituality"),
    GivingBack("Giving back"),
    Home("Home"),
    Attitude("Attitude"),
    Creativity("Creativity")
}

inline fun getWheelOfLifeValues(): List<String> {
    var aspects = mutableListOf<String>()


    for (value in WheelOfLife.values()) {
        aspects.add(value.aspect)
    }
    return aspects
}
package zm.co.goalgetter.planner.mapper

import zm.co.goalgetter.planner.common.WheelOfLife
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.model.GoalModel

class GoalMapper {
    fun mapToDomain(goal: GoalModel): Goal {
        return Goal(id = goal.id!!,
                aspect = goal.aspect ?: WheelOfLife.PersonalGrowth.aspect,
                specific = goal.specific!!,
                measurable = goal.measurable!!,
                attainable = goal.attainable!!,
                relevant = goal.relevant!!,
                timely = goal.timely!!,
                reward = goal.reward!!)
    }


    fun mapToDomainList(goalList: List<GoalModel>): List<Goal>{
        var domainList = mutableListOf<Goal>()
        goalList.forEach { goal ->
            domainList.add(mapToDomain(goal))
        }
        return domainList
    }
}
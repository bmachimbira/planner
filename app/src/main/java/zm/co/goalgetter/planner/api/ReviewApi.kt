package zm.co.goalgetter.planner.api

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.model.ReviewModel
import zm.co.goalgetter.planner.repository.ReviewRepository

class ReviewApi constructor(private var reviewRepository: ReviewRepository){

    fun saveReview(review: Review){
        reviewRepository.insert(review)
    }

    fun getAllReviews(): List<Review> {
        return reviewRepository.getAll()
    }

    fun getAllResults():RealmResults<ReviewModel>{
        return reviewRepository.getAllResults()
    }

    fun getReview(reviewId: String): Review {
        return reviewRepository.getById(reviewId)
    }

    fun updateReview(review: Review) {
        reviewRepository.update(review)
    }

    fun getReviewFromModel(reviewModel: ReviewModel?): Review {
        return reviewRepository.getReviewFromModel(reviewModel = reviewModel)
    }

}
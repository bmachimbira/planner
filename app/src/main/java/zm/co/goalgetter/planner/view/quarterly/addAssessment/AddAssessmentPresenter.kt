package zm.co.goalgetter.planner.view.quarterly.addAssessment

import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.common.getWheelOfLifeValues
import zm.co.goalgetter.planner.domain.Habit
import java.util.*

class AddAssessmentPresenter(val view: AddAssessmentContract.View, val habitApi: HabitApi) : AddAssessmentContract.UserActions {
    private var categories = getWheelOfLifeValues()

    override fun populateCategoryList(){
        view.populateCategoriesWithList(categories)
    }

    override fun saveAssessment(category: Int?,
                                rating: String?,
                                state: String?,
                                costToMe: String?,
                                obstacles: String?,
                                goal: String?,
                                newHabit: String?,
                                resourcesNeeded: String?) {
        val categorySelected = categories[category!!]
        val habit = Habit(
                id = Date().time.toString(),
                category = categorySelected,
                rating = rating,
                state = state,
                costToMe = costToMe,
                obstacles = obstacles,
                goal = goal,
                newHabit = newHabit,
                resourcesNeeded = resourcesNeeded)

        habitApi.saveHabit(habit = habit)
        view.endFragment()
    }
}
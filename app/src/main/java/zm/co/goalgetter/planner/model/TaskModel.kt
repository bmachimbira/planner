package zm.co.goalgetter.planner.model

import io.realm.RealmObject
import java.util.*

open class TaskModel : RealmObject(){
    var goalId:String? = null
    var title:String? = null
    var due: Date? = null
    var description: String? = null
}
package zm.co.goalgetter.planner.repository

import io.realm.DynamicRealm
import io.realm.RealmMigration

class RealmMigrations: RealmMigration {
    override fun migrate(realm: DynamicRealm?, oldVersion: Long, newVersion: Long) {
        val schema = realm!!.schema

        if (oldVersion < 2) {
            schema.get("GoalModel")!!.addField("aspect", String::class.java)
            schema.create("ReviewModel")
                    .addField("id", String::class.java)
                    .addField("goal", String::class.java)
                    .addField("performance", String::class.java)
                    .addField("highlights", String::class.java)
                    .addField("lessons", String::class.java)
                    .addField("obstacles", String::class.java)
                    .addField("howDoIDoBetter", String::class.java)
        }
    }
}
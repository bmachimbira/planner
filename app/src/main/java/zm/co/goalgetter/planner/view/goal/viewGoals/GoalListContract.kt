package zm.co.goalgetter.planner.view.goal.viewGoals

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.model.GoalModel

interface GoalListContract{
    interface View{
        fun setAdapter(goals: RealmResults<GoalModel>)
        fun showDetail(goal: Goal)

    }

    interface Presenter{

        fun initialiseList()
        fun loadGoal(goalId: String): Goal
        fun getItemAtIndex(position: Int)
        fun updateGoal(goalId: String, aspect: Int, timely: String, specific: String, relevant: String, measurable: String, attainable: String, reward: String)
    }
}
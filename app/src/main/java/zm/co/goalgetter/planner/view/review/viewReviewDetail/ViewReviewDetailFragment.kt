package zm.co.goalgetter.planner.view.review.viewReviewDetail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import kotlinx.android.synthetic.main.fragment_add_review.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.domain.Review

class ViewReviewDetailFragment : Fragment() {

    companion object {
        fun newInstance(review: Review): Fragment {
            val fragment = ViewReviewDetailFragment()
            fragment.review = review
            return fragment
        }
    }

    private lateinit var review: Review
    private lateinit var presenter: ViewReviewDetailContract.UserActions
    private lateinit var aspects: Spinner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_add_review, container, false)
        aspects = root.category
        loadDataIntoView(view = root)
        return root
    }

    private fun loadDataIntoView(view: View) {
        aspects.adapter = ArrayAdapter<String>(activity, R.layout.spinner_item, listOf(review.goal))
        view.performance.setText(review.performance)
        view.highlights.setText(review.highlights)
        view.lessons.setText(review.lessons)
        view.obstacles.setText(review.obstacles)
        view.howDoIDoBetter.setText(review.howDoIDoBetter)
    }

}
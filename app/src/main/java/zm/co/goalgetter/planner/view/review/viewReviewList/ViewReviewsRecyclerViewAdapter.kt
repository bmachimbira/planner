package zm.co.goalgetter.planner.view.review.viewReviewList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.goal_list_content.view.*
import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.model.ReviewModel

internal class ViewReviewsRecyclerViewAdapter(data: OrderedRealmCollection<ReviewModel>,
                                              val itemClickListener: (View, Int, Int) -> Unit) :
    RealmRecyclerViewAdapter<ReviewModel, ViewReviewsRecyclerViewAdapter.ReviewViewHolder>(data, true) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.goal_list_content, parent, false)
        val viewHolder = ReviewViewHolder(itemView)
        viewHolder.onClick(itemClickListener)
        return viewHolder
        return ReviewViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.goaltitle.text = data!![position].goal
        holder.dueDate.text = data!![position].performance.toString()
    }

    internal inner class ReviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var goaltitle: TextView
        var dueDate: TextView

        init {
            goaltitle = view.goalTitle
            dueDate = view.dueDate
        }
    }

    fun <T : RecyclerView.ViewHolder> T.onClick(event: (view: View, position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(it, getAdapterPosition(), getItemViewType())
        }
        return this
    }
}
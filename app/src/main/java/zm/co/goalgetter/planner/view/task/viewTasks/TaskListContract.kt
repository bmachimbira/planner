package zm.co.goalgetter.planner.view.task.viewTasks

import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Task
import zm.co.goalgetter.planner.model.TaskModel
import java.util.*

interface TaskListContract {

    interface View{
        fun loadTasksIntoAdapter(tasks: RealmResults<TaskModel>)
        fun  showDetail(task: Task)
    }

    interface Presenter{
        fun initialiseList()
        fun getItemAtIndex(position: Int)
        fun updateTask(goalSelected: String, taskTitle: String, description: String, dueDate: Date?)
        fun initialiseListForGoal(goalId: String)
    }
}
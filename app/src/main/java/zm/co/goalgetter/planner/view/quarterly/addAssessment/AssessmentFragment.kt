package zm.co.goalgetter.planner.view.quarterly.addAssessment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_assessment.view.*

import zm.co.goalgetter.planner.R
import zm.co.goalgetter.planner.api.HabitApi
import zm.co.goalgetter.planner.mapper.HabitMapper
import zm.co.goalgetter.planner.repository.HabitRepository

class AssessmentFragment : Fragment(), AddAssessmentContract.View {

    private lateinit var presenter: AddAssessmentContract.UserActions
    private lateinit var categoriesSpinner: Spinner

    companion object {
        fun newInstance(): AssessmentFragment {
            return AssessmentFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val habitApi = HabitApi(habitRepository = HabitRepository(realm = Realm.getDefaultInstance(), mapper = HabitMapper()))
        presenter = AddAssessmentPresenter(view = this, habitApi =  habitApi)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_assessment, container, false)

        this.categoriesSpinner = root.category

        presenter.populateCategoryList()

        root.saveHabitButton.setOnClickListener {
            presenter.saveAssessment(category = categoriesSpinner.selectedItemPosition,
                    rating = root.currentRating.text.toString(),
                    state = root.currentState.text.toString(),
                    costToMe = root.currentCost.text.toString(),
                    obstacles = root.obstacles.text.toString(),
                    goal = root.goal.text.toString(),
                    newHabit = root.newHabit.text.toString(),
                    resourcesNeeded = root.resourcesNeeded.text.toString()
            )
        }
        return root
    }

    override fun populateCategoriesWithList(categories: List<String>) {
        val adapter = ArrayAdapter<String>(activity, R.layout.spinner_item, categories)
        categoriesSpinner.adapter = adapter
    }

    override fun endFragment() {
        activity.onBackPressed()
    }
}

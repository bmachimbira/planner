package zm.co.goalgetter.planner.view.review.addReview

interface AddReviewContract {
    interface View {

        fun loadHabitsIntoSpinner(habits: List<String>)
        fun endFragment()
    }

    interface UserActions {
        fun loadAspectsIntoSpinner()
        fun saveReview(habit: Int, performance: String?, highlights: String?, lessons: String?, obstacles: String?, howDoIDoBetter: String?)

    }
}
package zm.co.goalgetter.planner.mapper

import zm.co.goalgetter.planner.domain.Review
import zm.co.goalgetter.planner.model.ReviewModel

class ReviewMapper {
    fun mapToDomain(reviewModel: ReviewModel): Review {
        return Review(
                id = reviewModel.id!!,
                goal = reviewModel.goal!!,
                performance = reviewModel.performance!!,
                highlights = reviewModel.highlights!!,
                lessons = reviewModel.lessons!!,
                obstacles = reviewModel.obstacles!!,
                howDoIDoBetter = reviewModel.howDoIDoBetter!!)
    }


    fun mapToDomainList(list: List<ReviewModel>): List<Review> {
        val domainList = mutableListOf<Review>()
        list.forEach { reviewModel ->
            domainList.add(mapToDomain(reviewModel))
        }
        return domainList
    }
}
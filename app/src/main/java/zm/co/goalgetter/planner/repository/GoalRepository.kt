package zm.co.goalgetter.planner.repository

import io.realm.Realm
import io.realm.RealmResults
import zm.co.goalgetter.planner.domain.Goal
import zm.co.goalgetter.planner.mapper.GoalMapper
import zm.co.goalgetter.planner.model.GoalModel

class GoalRepository constructor(var realm: Realm, var mapper: GoalMapper) : DataRepository<Goal> {
    fun getAllResults(): RealmResults<GoalModel> {
        return realm.where(GoalModel::class.java).findAll()
    }

    override fun getById(id: String): Goal {
        return mapper.mapToDomain(realm.where(GoalModel::class.java).equalTo("id", id).findFirst()!!)
    }

    override fun getAll(): List<Goal> {
        return mapper.mapToDomainList(realm.where(GoalModel::class.java).findAll())
    }

    override fun insert(value: Goal) {
        realm.executeTransaction {
            val goal: GoalModel = realm.createObject(GoalModel::class.java)
            goal.id = value.id!!
            goal.aspect = value.aspect
            goal.specific = value.specific!!
            goal.measurable = value.measurable!!
            goal.attainable = value.attainable!!
            goal.relevant = value.relevant!!
            goal.timely = value.timely!!
            goal.reward = value.reward!!
        }
    }

    override fun update(value: Goal) {
        realm.executeTransaction {
            val goal: GoalModel = realm.createObject(GoalModel::class.java)
            goal.id = value.id!!
            goal.aspect = value.aspect
            goal.specific = value.specific!!
            goal.measurable = value.measurable!!
            goal.attainable = value.attainable!!
            goal.relevant = value.relevant!!
            goal.timely = value.timely!!
            goal.reward = value.reward!!
        }
    }

    override fun delete(id: String) {
        realm.executeTransaction {
            val goal = realm.where(GoalModel::class.java).equalTo("id", id).findAll()
            goal.deleteAllFromRealm()
        }
    }

    override fun deleteAll() {
        realm.executeTransaction {
            realm.deleteAll()
        }
    }

    fun getGoalFromModel(goalModel: GoalModel?): Goal {
        return mapper.mapToDomain(goalModel!!)
    }
}
package zm.co.goalgetter.planner.view.goal.addGoal

interface AddGoalContract {

    interface View {
        fun loadAspects(aspects: List<String>)

        fun endFragment()
    }

    interface Presenter {
        fun addGoal(aspect: Int, timely: String, specific: String, measurable: String, attainable: String, relevant: String, reward: String)
        fun loadAspectsIntoSpinner()
    }
}